CC = gcc

SDIR = src
ODIR = obj
EXEC = exe

_OP = ./output

_SRC = main.c 1.c 2.c 3.c 4.c 5.c 6.c 7.c 8.c 9.c 10.c 11.c 12.c 13.c 14.c 15.c
SRC = $(patsubst %,$(SDIR)/%,$(_SRC))

_OBJ = main.o 1.o 2.o 3.o 4.o 5.o 6.o 7.o 8.o 9.o 10.o 11.o 12.o 13.o 14.o 15.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c -o $@ $<

$(EXEC)/output: $(OBJ)
	$(CC) -o $@ $^

.PHONY: clean

run:
	$(EXEC)/$(_OP)

clean:
	rm -f $(ODIR)/*.o $(EXEC)/*
