#ifndef STRING_H
#define STRING_H
#define SIZE 2048

/* 1.c */
void strcopy(char *, char *);

/* 2.c */
void strncopy(char *, char *, int);

/* 3.c */
char *chr_add_instr(char *, char);

/* 4.c */
char *sappend(char *, char *);

/* 5.c */
char *snappend(char *, char *, int);

/* 6.c */
char strcomp(char *, char *);

/* 7.c */
int strcasecomp(char *,char *);

/* 8.c */
int strspan(const char *, const char *);

/* 9.c */
char *strtoken(char *, char *);

/* 10.c */
int strpalin(char *, int);

/* 11.c */
void strrev(char *, int);

/* 12.c */
char *strsqeeze(char *);

/* 13.c */
int strrot(char *, char *);

/* 14.c */
char *remsstr(char *, char *);

/* 15.c */
char *insertchar(char *, char, int);

#endif
