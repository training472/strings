#include <string.h>

char *insertchar(char *str, char c, int pos)
{
	int len = strlen(str);

	static char ret[2048];
	int j = 0;

	if (*str != '\0' && len > pos) {
		for (int i = 0; i < pos-1; i++)
			ret[j++] = str[i];

		ret[j++] = c;

		for (int i = pos-1; i < len; i++)
			ret[j++] = str[i];
	}

	return ret;
}
