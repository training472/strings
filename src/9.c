char *strtoken(char *sbuf, char *delim)
{
	static char ret[1024];
	int i = 0;

	while (*sbuf != '\0') {
		if (*sbuf == *delim)
			break;
		else {
			ret[i] = *sbuf;
			i++;
			sbuf++;
		}
	}

	return ret;
}
