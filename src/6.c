char strcomp(char *str1, char *str2)
{
	char ret;

	while (*str1 == *str2 && *str1 != '\0' && *str2 != '\0') {
		str1++; str2++;
	}

	if (*str1 > *str2)
		ret = 1;
	else if (*str1 < *str2)
		ret = -1;
	else if (*str1 == *str2)
		 ret = 0;

	return ret;
}
