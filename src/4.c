char *sappend(char *str1, char *str2)
{
	char *ptr = str1;

	while (*ptr != '\0')
		ptr++;
	while (*str2 != '\0')
		*ptr++ = *str2++;

	*ptr = '\0';

	return str1;
}
