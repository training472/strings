int strpalin(char *str, int n)
{
	int flag = 1;

	for (int i = 0; i < n/2; i++) {
		if (str[i] != str[n - i - 1]) {
			flag = 0;
			break;
		}
			
	}

	return flag;
}
