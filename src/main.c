#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "../include/header.h"

int main(void)
{
	int ch;
	unsigned char choice[3];
	char sbuf[SIZE];
	char dbuf[SIZE];
	unsigned char N1[50];
	int N;
	char *ptr_char;
	char c[SIZE];
	char delim[SIZE];
	char in_c;
	int pos;

	do {
		printf("--------------------------------------\n");
		printf("String Programs\n");
		printf("--------------------------------------\n");

		printf("1. Given a string “Global Edge” in a character buffer sbuf, write a function void strcpy (char *dbuf, char *sbuf) to copy the string from sbuf to character buffer dbuf.\n");
		printf("2. Given a string “Global Edge” in a character buffer sbuf, write a function void strncpy(char *dbuf, char *sbuf, int n) to copy n characters from character buffer sbuf to character buffer dbuf.\n");
		printf("3. Given 2 parameters\n\ti. a string in a buffer buf\n\tii. a character in a variable ch\n\tfunction char * chr_add_instr (char *buf, char ch) that returns the address of the first occurrence of character ch in the given string buf.\n");
		printf("4. Given 2 strings str1 and str2, write a function char * sappend (char *str1, char *str2) to append string str1 to string str2. \n");
		printf("5. Given 2 strings str1 and str2, write a function char * snappend (char *str1, char *str2, int n) to append first n characters of string str1 to string str2.\n");
		printf("6. Given 2 strings str1 & str2, write a function char strcmp (char *str1, char * str2) to compare two strings str1 and str2 and returns the following\n\ti) 0 if two strings are equal \n\tii) 1 str1 is greater than str2\n\tiii) -1 if str1 is less than str2\n");
		printf("7. Given 2 strings in a character buffer str1 & str2. Write a function int strcasecmp(char *str1, char *str2) which compares two strings str1 and str2 by ignoring the case (lower or upper) and returns the following\n\ti) 0 if two strings are equal \n\tii) 1 str1 is greater than str2\n\tiii) -1 if str1 is less than str2\n");
		printf("8. Given 2 strings in a character buffer buf1 & buf2. Write a function size_t strspn(const char *buf1, const char *buf2) to count the initial set of characters in buf1, which matches any of the characters in buf2. It stops when a character in buf1 not found in buf2.\n");
		printf("9. Given 2 strings in a character buffer buf & delim. Write a function char * strtok(const char *buf, const char *delim) which parses buf until it encounters any of the delimiter in delim. For more info, read man page of strtok\n");
		printf("10. Program to check whether the given string is palindrome or not.\n");
		printf("11. Program to reverse a given string.\n");
		printf("12. Program to squeeze the consecutive similar characters in a given string.\n");
		printf("13. Given 2 strings str and rstr, write a funtion int strrot(const char *str, const char *rstr) to check whether rstr is a rotated string of str. \n");
		printf("14. Given 2 strings str and sstr, write a function char *remsstr(char *str, const char *sstr), which checks for the substring sstr in str and if found removes the substring sstr in string str and returns the modified string str. If the substring sstr is not found, it returns the original string str\n");
		printf("15. Given a string str, write a function char * insertchar (char *str, const char ch, int pos) which will insert a char ch in the given string str at a specified position pos.\n");

		printf("\nENTER 0 TO EXIT\n\n");
		printf("Enter your choice: ");

		fgets(choice, 3, stdin);
		ch = atoi(choice);

		printf("\n");

		switch (ch) {
			case 0 : exit(0);

			case 1 : printf("Enter the string: ");
				 
				 fgets(sbuf, SIZE, stdin);
				
				 if (strlen(sbuf) > 0)
				 	strcopy(dbuf, sbuf);

				 printf("\nCopied string: %s \n\n", dbuf);

				 break;

			case 2 : printf("Enter the string: ");
				 fgets(sbuf, SIZE, stdin);

				 printf("Enter the number of characters to be copied: ");
				 fgets(N1, 50, stdin);
				 N = atoi(N1);

				 if (strlen(sbuf) > 0 && strlen(sbuf) >= N)
				 	strncopy(dbuf, sbuf, N);
				 else
					 printf("Invalid input!!!\n\n");

				 printf("Copied string: %s \n\n", dbuf);

				 break;

			case 3 : printf("Enter the string: ");
				 fgets(sbuf, SIZE, stdin);

				 printf("Enter the character: ");
				 fgets(c, SIZE, stdin);
				 char schar = c[0];

				 if (strlen(sbuf) > 0 && sbuf[0] != '\0')
				 	ptr_char = chr_add_instr(sbuf, schar);
				 else
					 printf("Invalid input...\n\n");
				
				 printf("%c is at address: %p\n\n", schar, ptr_char);

				 break;

			case 4 : printf("Enter the first string: ");
				 //fgets(sbuf, SIZE, stdin);
				 scanf("%s", sbuf);
				 printf("Enter the second string: ");
				 //fgets(dbuf, SIZE, stdin);
				 scanf("%s", dbuf);

				 char *res = sappend(sbuf, dbuf);

				 printf("Appended string: %s\n\n", res);

				 break;

			case 5 : printf("Enter the first string: ");
                                 //fgets(sbuf, SIZE, stdin);
                                 scanf("%s", sbuf);
                                 printf("Enter the second string: ");
                                 //fgets(dbuf, SIZE, stdin);
                                 scanf("%s", dbuf);

				 printf("Enter the number of characters to append: ");
				 scanf("%d", &N);

				 char *res_n;
				 if (strlen(dbuf) > 0 && strlen(dbuf) >= N)
                                 	res_n = snappend(sbuf, dbuf, N);
				 else
					 printf("Invalid input!!!\n\n");

                                 printf("Appended string: %s\n\n", res_n);

				 break;

			case 6 : printf("Enter the first string: ");
                                 //fgets(sbuf, SIZE, stdin);
                                 scanf("%s", sbuf);
                                 printf("Enter the second string: ");
                                 //fgets(dbuf, SIZE, stdin);
                                 scanf("%s", dbuf);

				 char ret = strcomp(sbuf, dbuf);

				 if (ret == 0)
					 printf("\nStrings are equal... \n\n");
				 else if (ret == 1)
					 printf("\n%s is greater than %s\n\n", sbuf, dbuf);
				 else if (ret == -1)
					 printf("\n%s is breater than %s\n\n", dbuf, sbuf);

				 break;

			case 7 : printf("Enter the first string: ");
                                 //fgets(sbuf, SIZE, stdin);
                                 scanf("%s", sbuf);
                                 printf("Enter the second string: ");
                                 //fgets(dbuf, SIZE, stdin);
                                 scanf("%s", dbuf);

                                 int ret_case = strcasecomp(sbuf, dbuf);

                                 if (ret_case == 0)
                                         printf("\nStrings are equal... \n\n");
                                 else if (ret_case == 1)
                                         printf("\n%s is greater than %s\n\n", sbuf, dbuf);
                                 else if (ret_case == -1)
                                         printf("\n%s is greater than %s\n\n", dbuf, sbuf);

                                 break;

			case 8: printf("Enter the first string: ");
                                 fgets(sbuf, SIZE, stdin);
                                 //scanf("%s", sbuf);
                                 printf("Enter the second string: ");
                                 fgets(dbuf, SIZE, stdin);
                                 //scanf("%s", dbuf);
				
				 printf("Character count: %d\n\n", strspan(sbuf, dbuf));

				 break;

			case 9 : printf("Enter the string: ");
				 fgets(sbuf, SIZE, stdin);
				 printf("Enter the delimiter: ");
				 fgets(delim, SIZE, stdin);

				 printf("String after strtok(): %s\n\n", strtoken(sbuf, delim));

				 break;

			case 10 : printf("Enter the string: ");
				  //fgets(sbuf, SIZE, stdin);
				  scanf("%s", sbuf);

				  printf("\n");

				  if (strpalin(sbuf, strlen(sbuf)))
					  printf("%s is palindrome...\n\n", sbuf);
				  else
					  printf("%s is not a palindrome...\n\n", sbuf);

				 break;

			case 11 : printf("Enter the string: ");
				  scanf("%s", sbuf);
				  //fgets(sbuf, SIZE, stdin);

				  strrev(sbuf, strlen(sbuf));

				  printf("String after reversing: %s\n\n", sbuf);

				  break;

			case 12 : printf("Enter the string: ");
				  //fgets(sqeez, SIZE, stdin);
				  scanf("%s", sbuf);

				  printf("Sqeezed string: %s\n\n", strsqeeze(sbuf));
				  
				  break;

			case 13 : printf("Enter the string: ");
				  //fgets(sbuf, SIZE, stdin);
				  scanf("%s", sbuf);
				  printf("Enter the reverse string: ");
				  //fgets(dbuf, SIZE, stdin);
				  scanf("%s", dbuf);

				  int ret_rot = strrot(sbuf, dbuf);

				  if (ret_rot)
					  printf("\nEntered string is rotated...\n\n");
				  else
					  printf("\nEntered string is not rotated...\n\n");

				  break;

			case 14 : printf("Enter the string: ");
				  scanf("%s", sbuf);
				  printf("Enter the substring: ");
				  scanf("%s", dbuf);

				  char *ret_str = remsstr(sbuf, dbuf);

				  printf("\nModified string: %s\n\n", ret_str);

				  break;

			case 15 : printf("Enter the string: ");
                                  scanf("%s", sbuf);
				  //fgets(sbuf, SIZE, stdin);
				  printf("Enter the character: ");
				  char in_c[SIZE];
				  //fgets(in_c, SIZE, stdin);
				  scanf("%s", in_c);
				  printf("Enter the position to insert: ");
				  scanf("%d", &pos);

				  char *ret_instr = insertchar(sbuf, in_c[0], pos);

				  printf("String after modification: %s\n\n", ret_instr);
				  
				  break;

			default : printf("Invalid choice!!!\n\n");
		}

	} while(1);

	return 0;
}
