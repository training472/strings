#include <string.h>

char *remsstr(char *str, char *sstr)
{
	int count = 0;
	int i = 0;

	static char ret[2048];

	char *ptr = str;
	char *sptr = sstr;

	int len = strlen(str);
	int slen = strlen(sstr);

	while (*ptr != '\0') {
		if (*ptr == *sptr) {
			*ptr++;
			*sptr++;
			count++;
		}
		else {
			ret[i++] = *ptr;
			*ptr++;
		}
	}

	return ret;
}
