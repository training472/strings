#include <string.h>

int strrot(char *str, char *rstr)
{
	int ret = 0;
	int count = 0;
	
	int slen = strlen(str);
	strcat(str, str);
	int rlen = strlen(rstr);
	
	if (slen == rlen) {
		while (*str != '\0' && *rstr != '\0') {
			if (*str == *rstr) {
				str++;
				rstr++;
				count++;
			} else 
				str++;
		}
		if (count == rlen)
			ret = 1;
		else
			ret = 0;
	}

	return ret;
}
