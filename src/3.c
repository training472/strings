char *chr_add_instr(char *buf, char c)
{
	int i = 0;

	while (*(buf + i) != '\0') {
		if (*(buf + i) == c)
			break;
		else 
			i++;
	}

	return (char *)(buf + i);
}
