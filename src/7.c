#include <ctype.h>
int strcasecomp(char *str1, char *str2)
{
	int ret;

	while ((toupper(*str1) == toupper(*str2)) && (tolower(*str1) == tolower(*str2)) && *str1 != '\0' && *str2 != '\0') {
		str1++;
		str2++;
	}

	if ((toupper(*str1) == toupper(*str2)) && (tolower(*str1) == tolower(*str2)))
		ret = 0;
	else if ((toupper(*str1) > toupper(*str2)) && (tolower(*str1) > tolower(*str2)))
		ret = 1;
	else if ((toupper(*str1) < toupper(*str2)) && (tolower(*str1) < tolower(*str2)))
		ret = -1;

	return ret;
}
