char *snappend(char *str1, char *str2, int n)
{
	char *ptr = str1;

	while (*ptr != '\0')
		ptr++;

	for (int i = 0; i < n; i++)
		*ptr++ = *str2++;

	*ptr = '\0';

	return str1;
}
